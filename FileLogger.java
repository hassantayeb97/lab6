package org.example;

public class FileLogger extends BaseLogger{
    public FileLogger() {
        label = "Console logger";
    }

    public String getLevel() {
        return "info";
    }
}
