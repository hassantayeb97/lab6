Hassan Asim Tayeb 1936820

Q2 answer:
The first solution caused class explosion in purpose of covering every possible and reasonable combination of added functions to an object, and any change in the code will break all the code.

The second solution was better than the first one, but caused to forcly add non-reasonable functions to every object.

The third attempt enables you to add and use functions with an object without any other restrictions exist in the first two attempts. You can add a new function separately by only adding its class, and without changing any other classes exist in code structure. You can use multiple functions in one object by simply adding the function itself to the object whenever you want, and the implementation will simply change accordingly. 
